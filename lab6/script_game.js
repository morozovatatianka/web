var yellowCheker = "https://upload.wikimedia.org/wikipedia/commons/8/82/Circle-yellow.svg";
var whiteCheker = "https://upload.wikimedia.org/wikipedia/commons/5/58/White_Circle.svg";
var blackCheker = "https://upload.wikimedia.org/wikipedia/commons/2/27/Disc_Plain_black.svg";

var whiteKing = "https://upload.wikimedia.org/wikipedia/commons/a/a0/Circle_-_black_simple.svg?uselang=ru";
var blackKing = "https://upload.wikimedia.org/wikipedia/commons/2/25/Map_pointer_black.svg";
var yellowKing = "https://upload.wikimedia.org/wikipedia/commons/9/96/Dot-yellow.svg";

var blackCell = '#8B4513';
var greenCell = '#7FFFD4';
var redCell = '#FF6347';

var idWrittingMoving = "rule";

//включен ли режим подсказки
var isCheked = false;
//id выбранной шашки
var idCheker = "";

let characters = ["A", "B", "C", "D", "E", "F", "G", "H"]
//модели расстановки, которые нужно вывести при нажатии на кнопки
let boardBegin = [
	["", "b1", "", "b2", "", "b3", "", "b4"],
	["b5", "", "b6", "", "b7", "", "b8", ""],
	["", "b9", "", "b10", "", "b11", "", "b12"],
	["", "", "", "", "", "", "", ""],
	["", "", "", "", "", "", "", ""],
	["w12", "", "w11", "", "w10", "", "w9", ""],
	["", "w8", "", "w7", "", "w6", "", "w5"],
	["w4", "", "w3", "", "w2", "", "w1", ""]
];

let boardExample = [
	["", "b1", "", "", "", "", "", ""],
	["", "", "b4", "", "b5", "", "", ""],
	["", "", "", "", "", "", "", "b6"],
	["", "", "b3", "", "", "", "", ""],
	["", "", "", "", "", "w1", "", "w2"],
	["", "", "", "", "", "", "", ""],
	["", "", "", "", "", "", "", ""],
	["", "", "b2", "", "", "", "", ""]
];

var map = new Map();

//Model of chekers
let board = [
	["", "b1", "", "b2", "", "b3", "", "b4"],
	["b5", "", "b6", "", "b7", "", "b8", ""],
	["", "b9", "", "b10", "", "b11", "", "b12"],
	["", "", "", "", "", "", "", ""],
	["", "", "", "", "", "", "", ""],
	["w12", "", "w11", "", "w10", "", "w9", ""],
	["", "w8", "", "w7", "", "w6", "", "w5"],
	["w4", "", "w3", "", "w2", "", "w1", ""]
];

//ход белых
var moveWhite = true;

//можно завершить ход
var canFinish = false;

//можно ходить
var canMoving = true;

//номер хода
var numMoving = 1;

// нажали на кнопку "Начало"
function begin() {
clearCurrentBoard();
copyElem(boardBegin);
clearMovingSides();
}

// нажали на кнопку "Пример 1"
function example1() {
clearCurrentBoard();
var queen = map.get(boardExample[7][2]);
queen.src = blackKing;
copyElem(boardExample);
clearMovingSides();
}

// приводим доску к начальному состоянию
function clearCurrentBoard() {
	for (let i = 0; i < board.length; i++) {
		for (let j = 0; j < board.length; j++) {
			if (board[i][j] != "") {
				var id = characters[j] + (board.length - i);
				var elem = document.getElementById(id);
				var img = document.getElementById(board[i][j]);
				map.set(board[i][j], img);
				if (board[i][j][0] == 'b') {
					img.src = blackCheker;	
				} else {
					img.src = whiteCheker;
				}
 				elem.removeChild(img);
			}
		}
	}
clearFields();
clearMoving();
isCheked = false;
idCheker = "";
moveWhite = true;
canFinish = false;
canMoving = true;
isRedMoving = false;
}

// переносим в доску, отображающую текущее состояние,
// необходимую расстановку шашек
function copyElem(array) {
for (let i = 0; i < board.length; i++) {
	for (let j = 0; j < board.length; j++) {
			board[i][j] = array[i][j];
	}
}

for (let i = 0; i < board.length; i++) {
	for (let j = 0; j < board.length; j++) {
			if (board[i][j] != "") {
				var id = characters[j] + (board.length - i);
				var elem = document.getElementById(id);
				var img = map.get(board[i][j]);
				map.delete(board[i][j]);
 				elem.appendChild(img);
			}
	}
}
}

//список всех обязательных и возможных полей текущей шашки
let redFields = new Array();
let greenFields = new Array();

// нажимаем на шашку
function clickCheker(id) {
changeCheker(id);
}

//старые ходы
let oldMoving = new Map();
//нажимаем на ячейку
function clickCell(id){
if (!isCheked) {
	return;
}
var cell = document.getElementById(id);
var colorGreen = "rgb(127, 255, 212)";
var colorRed = "rgb(255, 99, 71)";

if (cell.style.backgroundColor == colorRed) {
	doRedMoving(id);
}

if (cell.style.backgroundColor == colorGreen) {
	doGreenMoving(id);
}

}

function doGreenMoving(id) {
var cell = document.getElementById(id);
var cheker = document.getElementById(idCheker);
//записала, где стояла ячейка до хода
var parentId = cheker.parentElement.id;

//записываю положение шашек в начле хода, только тех, которые были изменены
//при нажатии завершить ход записываем все изменения в map и 
oldMoving.set(cheker, document.getElementById(parentId));

//местоположение шашки на board
for (let i = 0; i < characters.length; i++) {
	if (characters[i] == parentId[0]) {
		var oldCol = i; 
	}

	if (characters[i] == id[0]) {
		var newCol = i; 
	}
	
}
var oldStr = 8 - parentId[1];
var newStr = 8 - id[1];

//	поменять положение шашки в массиве board
board[newStr][newCol] = board[oldStr][oldCol];
board[oldStr][oldCol] = "";

// перемещаем шашку
cell.appendChild(cheker);

doKing(newStr, cheker);

//делаем шашку не выделенной
doUncheking(cheker, idCheker);
// убираем все подсказки с полей
clearFields();
// отмечаем, что больше нет выделенной шашки и можем завершить ход
//и не можем больше походить этой стороной
isCheked = false;
idCheker = "";
canFinish = true;
canMoving = false;

writtingMoving(parentId, cell.id, false);
}

var isBegin = true;
function doRedMoving(id) {
var cell = document.getElementById(id);
//убираем шашку, которую срубили
	var cuttingCheker = document.getElementById(cuttingChekerId);
	var cellCuttingChekerId = cuttingCheker.parentElement.id;

	var strBoard = 8 - cellCuttingChekerId[1];
	var colBoard;
	for (let i = 0; i < characters.length; i++) {
		if(cellCuttingChekerId[0] == characters[i]) {
			colBoard = i;
			break;
		}
	}

	board[strBoard][colBoard] = "";
	
	var cellCuttingCheker = document.getElementById(cellCuttingChekerId);
	if (cuttingChekerId[0] == 'b') {
		cuttingCheker.src = blackCheker;	
	} else {
		cuttingCheker.src = whiteCheker;
	}
	map.set(cuttingChekerId, cuttingCheker);
	oldMoving.set(cuttingCheker, cellCuttingCheker);
 	cellCuttingCheker.removeChild(cuttingCheker);

var cheker = document.getElementById(idCheker);
//записала, где стояла ячейка до хода
var parentId = cheker.parentElement.id;
//записываю положение шашек в начале хода, только тех, которые были изменены
//при нажатии завершить ход записываем все изменения в map и 
if (!oldMoving.has(cheker)) {
	oldMoving.set(cheker, document.getElementById(parentId));
}
//местоположение шашки на board
for (let i = 0; i < characters.length; i++) {
	if (characters[i] == parentId[0]) {
		var oldCol = i; 
	}

	if (characters[i] == id[0]) {
		var newCol = i; 
	}

	
}
var oldStr = 8 - parentId[1];
var newStr = 8 - id[1];

//	поменять положение шашки в массиве board
board[newStr][newCol] = board[oldStr][oldCol];
board[oldStr][oldCol] = "";

// перемещаем шашку
cell.appendChild(cheker);
doKing(newStr, cheker);
clearFields();
//ищем обязательные ходы
highlightRequired(idCheker);
// если обязательных ходов не нашлось
if (redFields.length == 0) {
	doUncheking(cheker, idCheker);	
	// убираем все подсказки с полей
	clearFields();
// отмечаем, что больше нет выделенной шашки
	isCheked = false;
	idCheker = "";
	canMoving = false;
	writtingMoving(parentId, cell.id, true);
	canFinish = true;
	isBegin = true;
	return;
}

writtingMoving(parentId, cell.id, true);
isBegin = false;
}


var currentCheker;
// осуществляем переход в режим подсказки и обратно
function changeCheker(id) {
var cheker = document.getElementById(id);
if (((isBlack(cheker) && !moveWhite) || 
	(isWhite(cheker) && moveWhite))
 && canMoving) { // если шашка не выделена и ход данной стороны
	if (isCheked) { //и нет уже выделенной
	return;
	}
// то выделяем текущую  
	doCheking(cheker);
// ищем обязательные ходы у выделенной шашки и отмечаем их
	highlightRequired(id);
// если обязательных ходов не нашлось, то ищем возможные и их отмечаем
	if (redFields.length == 0) {
		isRedMoving = false;
		checkRedMoving();
		if (!isRedMoving) {
			unrequiredMove(id);
		}
	}
// запоминаем, что уже выделили одну шашку
	isCheked = true;
	idCheker = id;
} else if (isCheking(cheker)) { // если текущая шашка уже выделена
//делаем шашку не выделенной
	doUncheking(cheker, id);
// убираем все подсказки с полей
	clearFields();
// отмечаем, что больше нет выделенной шашки
	isCheked = false;
	idCheker = "";
}
}

// данная фигура дамка?
function isKing(cheker) {
	return (cheker.src == blackKing || cheker.src == whiteKing || 
cheker.src == yellowKing);
}

//черная фигура?
function isBlack(cheker) {
	return cheker.src == blackKing || cheker.src == blackCheker;
}

// белая фигура?
function isWhite(cheker) {
	return cheker.src == whiteKing || cheker.src == whiteCheker;
}

// фигура в режиме подсказки?
function isCheking(cheker) {
	return cheker.src == yellowKing || cheker.src == yellowCheker;
}

// переводим шашку в режим подсказки
function doCheking(cheker) {
	if (isKing(cheker)) {
		cheker.src = yellowKing;
	} else {
		cheker.src = yellowCheker;
	}
}

// ищем все возможные ходы данной фигуры
function unrequiredMove(id) {
var cheker = document.getElementById(id);
var parentId = cheker.parentElement.id;

var row = 8 - parentId.charAt(1);
var col = getCol(parentId);

if (isKing(cheker)) {
	greenFieldsForKing(row, col);
} else {
	greenFieldsForCheker(row, col, id);
}

}

//ищем и выделяем все возможные ходы обычной шашки
//просматриваем все диагонали для хода вперёд
function greenFieldsForCheker(row, col, id) {
if (id[0] == 'b' && row < 7 && col > 0 && board[row + 1][col - 1] == "") {
	doGreen(col - 1, 7 - row);
}

if (id[0] == 'b' && col < 7 && row < 7 && board[row + 1][col + 1] == "") {
	doGreen(col + 1, 7 - row);
}

if (id[0] == 'w' && col > 0 && row > 0 && board[row - 1][col - 1] == "") {
	doGreen(col - 1, 9 - row);
}

if (id[0] == 'w' && col < 7 && row > 0 && board[row - 1][col + 1] == ""){
	doGreen(col + 1, 9 - row);
}

}

// просматриваем все диагонали у дамки и отмечаем все возможные ходы
function greenFieldsForKing(row, col) {
	topLeftGreenDiagonal(row, col);
	topRightGreenDiagonal(row, col);
	bottomLeftGreenDiagonal(row, col);
	bottomRightGreenDiagonal(row, col);
}


// смотрим обязательные ходы
function highlightRequired(id) {
var cheker = document.getElementById(id);
var parentId = cheker.parentElement.id;
var parent = document.getElementById(parentId);

var row = 8 - parentId.charAt(1);
var col = getCol(parentId);

//шашка для срубания
var item = '';
if (id[0] == 'b') {
	item = 'w';
} else {
	item = 'b';
}

if (isKing(cheker)) {
	redFieldsForKing(row, col, id, item);
} else {
	redFieldsForCheker(row, col, item);
}

}

var cuttingChekerId = "";
//смотрим есть ли обязательные ходы у шашки
function redFieldsForCheker(row, col, item) {
if (col > 1 && row < 6 && board[row + 1][col - 1].charAt(0) == item && 
	board[row + 2][col - 2] == "") {
	doRed(col - 2, 6 - row);
	cuttingChekerId = board[row + 1][col - 1];
}

if (col < 6 && row < 6 && board[row + 1][col + 1].charAt(0) == item &&
	board[row + 2][col + 2] == "") {
	doRed(col + 2, 6 - row);
	cuttingChekerId = board[row + 1][col + 1];
}

if (col > 1 && row > 1 && board[row - 1][col - 1].charAt(0) == item &&
	board[row - 2][col - 2] == "") {
	doRed(col - 2, 10 - row);
	cuttingChekerId = board[row - 1][col - 1];
}

if (col < 6 && row > 1 && board[row - 1][col + 1].charAt(0) == item &&
	board[row - 2][col + 2] == ""){
	doRed(col + 2, 10 - row);
	cuttingChekerId = board[row - 1][col + 1];
}

}

//просматриваем все диагонали на наличие обязательных ходов у дамки
function redFieldsForKing(row, col, id, item) {
	topLeftDiagonal(row, col, id, item);
	topRightDiagonal(row, col, id, item);
	bottomLeftDiagonal(row, col, id, item);
	bottomRightDiagonal(row, col, id, item);
}

function topLeftDiagonal(row, col, id, item) {
if (col <= 1 || row <= 1) {
	return;
}
var curRow = row - 1;
var curCol = col - 1;
var elemTopLeft;
var isCutting = false;

//проверка по диагонали верхней левой
while (curCol >= 0 && curRow >= 0) {
	elemTopLeft = board[curRow][curCol];
	if (elemTopLeft[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemTopLeft != "") {
			return;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemTopLeft[0] == item && curRow != 0 && curCol != 0) {
		
		if (board[curRow - 1][curCol - 1] != "") {
			return;
		}
		cuttingChekerId = elemTopLeft;
		isCutting = true;
	}
	curRow--;
	curCol--;
}
}

function topRightDiagonal(row, col, id, item) {
if (col >= 6 || row <= 1) {
	return;
}
var curRow = row - 1;
var curCol = col + 1;
var elemTopRight;
var isCutting = false;

//проверка по диагонали верхней правой
while (curCol <= 7 && curRow >= 0) {
	elemTopRight = board[curRow][curCol];

	if (elemTopRight[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemTopRight != "") {
			return;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemTopRight[0] == item && curRow != 0 && curCol != 7) {
		if (board[curRow - 1][curCol + 1] != "") {
			return;
		}
		cuttingChekerId = elemTopRight;
		isCutting = true;
	}
	curRow--;
	curCol++;
}
}

function bottomRightDiagonal(row, col, id, item) {
if (col >= 6 || row >= 6) {
	return;
}
var curRow = row + 1;
var curCol = col + 1;
var elemBottomRight;
var isCutting = false;

//проверка по диагонали нижняя правой
while (curCol <= 7 && curRow <= 7) {
	elemBottomRight = board[curRow][curCol];

	if (elemBottomRight[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemBottomRight != "") {
			return;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemBottomRight[0] == item && curRow != 7 && curCol != 7) {
		if (board[curRow + 1][curCol + 1] != "") {
			return;
		}
		cuttingChekerId = elemBottomRight;
		isCutting = true;
	}
	curRow++;
	curCol++;
}
}

function bottomLeftDiagonal(row, col, id, item) {
if (col <= 1 || row >= 6) {
	return;
}
var curRow = row + 1;
var curCol = col - 1;
var elemBottomLeft;
var isCutting = false;

//проверка по диагонали нижняя левая
while (curCol >= 0 && curRow <= 7) {
	elemBottomLeft = board[curRow][curCol];
	if (elemBottomLeft[0] == id[0]) {
		return;
	}
	if (isCutting) {
		if (elemBottomLeft != "") {
			return;
		} 
		doRed(curCol, 8 - curRow);
	}
//срубаемый элемент на диагонале
	if (elemBottomLeft[0] == item && curRow != 7 && curCol != 0) {
		
		if (board[curRow + 1][curCol - 1] != "") {
			return;
		}
		cuttingChekerId = elemBottomLeft;
		isCutting = true;
	}
	curRow++;
	curCol--;
}
}

function topLeftGreenDiagonal(row, col) {
if (col == 0 || row == 0) {
	return;
}
var curRow = row - 1;
var curCol = col - 1;
var elemTopLeft;
while (curCol >= 0 && curRow >= 0) {
	elemTopLeft = board[curRow][curCol];
	if (elemTopLeft != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow--;
	curCol--;
}
	
}

function topRightGreenDiagonal(row, col) {
if (col == 7 || row == 0) {
	return;
}
var curRow = row - 1;
var curCol = col + 1;
var elemTopRight;
while (curCol <= 7 && curRow >= 0) {
	elemTopRight = board[curRow][curCol];
	if (elemTopRight != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow--;
	curCol++;
}
}

function bottomLeftGreenDiagonal(row, col) {
if (col == 0 || row == 7) {
	return;
}
var curRow = row + 1;
var curCol = col - 1;
var elemTopRight;
while (curCol >=0 && curRow <= 7) {
	elemTopRight = board[curRow][curCol];
	if (elemTopRight != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow++;
	curCol--;
}
}

function bottomRightGreenDiagonal(row, col) {
if (col == 7 || row == 7) {
	return;
}
var curRow = row + 1;
var curCol = col + 1;
var elemTopRight;
while (curCol <= 7 && curRow <= 7) {
	elemTopRight = board[curRow][curCol];
	if (elemTopRight != "") {
		return;
	}
	doGreen(curCol, 8 - curRow);
	curRow++;
	curCol++;
}
}


//делаем обязательные поля красными
function doRed(col, row) {
	var itemId = characters[col] + (row);
	var item = document.getElementById(itemId);
	redFields.push(itemId);
	item.style.backgroundColor  = redCell;
}

//делаем возможные поля зелёными
function doGreen(col, row) {
	var itemId = characters[col] + (row);
	var item = document.getElementById(itemId);
	greenFields.push(itemId);
	item.style.backgroundColor  = greenCell;
}

// получаем текущую букву на доске у шашки
function getCol(parentId) {
for (let i = 0; i < characters.length; i++) {
	if (characters[i] == parentId.charAt(0)) {
		return i;
	}
}
}

// убираем режим подсказки с шашки
function doUncheking(cheker, id) {
if (!isKing(cheker)) {
	if (id[0] == 'b') {
		cheker.src = blackCheker;	
	} else {
		cheker.src = whiteCheker;
	}
} else {
	if (id[0] == 'b') {
		cheker.src = blackKing;	
	} else {
		cheker.src = whiteKing;
	}
}
}

// убираем режим подсказки с ячеек доски
//и очищаем список выделенных полей
function clearFields() {
while (redFields.length > 0) {
	var item = document.getElementById(redFields.pop());
	item.style.backgroundColor = '#8B4513';
}

while (greenFields.length > 0) {
	var item = document.getElementById(greenFields.pop());
	item.style.backgroundColor = '#8B4513';
}
}

function clearMoving() {
oldMoving.clear()
}

function finishMove() {
if (!canFinish) {
	return;
}
if (moveWhite) {
	moveWhite = false;
} else {
	moveWhite = true;
}

canFinish = false;
canMoving = true;
wasCheker = false;
isRedMoving = false;

addMoving();
clearMoving();
}

function cancelMove(id) {
oldMoving.forEach((value, key) => {
	if (map.has(key.id)) {
		for (let i = 0; i < characters.length; i++) {
			if (characters[i] == value.id[0]) {
				var oldCol = i; 
			}
		}
		var oldStr = 8 - value.id[1];
		board[oldStr][oldCol] = key.id;
		value.appendChild(key);
	} else {
		var parentId = key.parentElement.id;

		//местоположение шашки на board
		for (let i = 0; i < characters.length; i++) {
			if (characters[i] == parentId[0]) {
				var oldCol = i; 
			}

			if (characters[i] == value.id[0]) {
				var newCol = i; 
			}
	
		}
		var oldStr = 8 - parentId[1];
		var newStr = 8 - value.id[1];

		//поменять положение шашки в массиве board
		board[newStr][newCol] = board[oldStr][oldCol];
		board[oldStr][oldCol] = "";

		// перемещаем шашку
		value.appendChild(key);	

		if (wasCheker) {
			if (key.id[0] == 'w') {
				key.src = whiteCheker;
			} else {
				key.src = blackCheker;
			}
		}
		wasCheker = false;
	}
});
clearMoving();
canFinish = false;
canMoving = true;
moving = "";
var ch = document.getElementById(idCheker);
doUncheking(ch, idCheker)
isCheked = false;
clearFields();
}


var moving = "";
function writtingMoving(beginCell, endCell, isCutting) {
	if (moveWhite && isBegin) {
		moving += "<br>";
		moving += numMoving + ". ";
		 numMoving++;
	}

	moving += beginCell;

	if (isCutting) {
		moving += ":";
	} else {
		moving += "-";
	}

	if (!canMoving) {
		moving += endCell;
		moving += "  ";
	}
	
}

function addMoving() {
	var text = document.getElementById(idWrittingMoving);
	text.innerHTML += moving;
	moving = "";
	changeSide();
}

function clearMovingSides() {
	changeSide();
	var text = document.getElementById(idWrittingMoving);
	text.innerHTML = "Запись ходов: ";
	moving = "";
}

function changeSide() {
	var text = document.getElementById("side");
	if (moveWhite) {
		text.innerHTML = " Ход белых ";
	} else {
		text.innerHTML = " Ход чёрных ";
	}
	
}

var wasCheker = false;
function doKing(newStr, cheker) {
if (newStr == 0 && moveWhite && cheker.src == yellowCheker) {
	if (isCheked) {
	cheker.src = "https://www.flaticon.com/svg/static/icons/svg/864/864640.svg";
	} else {
	cheker.src = "https://www.flaticon.com/svg/static/icons/svg/606/606032.svg";
	}
wasCheker = true;
}

if (newStr == 7 && !moveWhite && cheker.src == yellowCheker) {
	if (isCheked) {
	cheker.src = "https://www.flaticon.com/svg/static/icons/svg/606/606114.svg";
	} else {
	cheker.src = "https://www.flaticon.com/svg/static/icons/svg/606/606032.svg";
	}
wasCheker = true;
}

}

var isRedMoving = false;
function checkRedMoving() {
for (let i = 0; i < board.length; i++) {
	for (let j = 0; j < board.length; j++) {
		// ищем обязательные ходы у выделенной шашки и отмечаем их
		if ((moveWhite && board[i][j][0] == "w") || (!moveWhite && board[i][j][0] == "b")) {
			highlightRequired(board[i][j]);
		// если обязательных ходов не нашлось, то ищем возможные и их отмечаем
			if (redFields.length != 0) {
				isRedMoving = true;
				clearFields();
				return;
			}
		}
	}
}

}





//	Декодер партии

function show() {
numMoving = 1;
begin();
var party = document.getElementById("input");
var text = party.value;

var num = 1;
var numParty = num+".";
var place = text.indexOf(numParty);

if (!hasFirstParty(num, place)) {
	alert("Нет первого хода партии");
	return;
}

text = text.slice(place, text.length)

let arrayParty = text.split("\n");

//пока есть следующий код
while(place != -1) {
	if (place == '' || !isCorrectParty(place, num, arrayParty[num-1])) {
		if (place == '') {num--;}
		alert("Разбор партии закончился на " + num + " ходе.");
		return;
	}

	num++;
	numParty = num+".";
	place = arrayParty[num-1].startsWith(numParty);
}

}

function hasFirstParty(num, place) {
	if (num == 1 && place == -1) {
		return false;
	}
return true;
}

function isCorrectParty(place, num, party) {
if (party.startsWith(place) == -1) {
	return false;
}

//убираем номер партии и пробелы по краям, заменяем неско-ко пробелов на 1
let strNum = `${num}`;
var leng = (strNum.length+1);

var moving = party.slice(leng, party.length);
moving = moving.trim();
moving = moving.replace(/\s+/g, ' ');

var arrayMoving = moving.split(" ");

if (arrayMoving.length > 2 || arrayMoving.length < 1) {
	return false;
}

var whiteMoving = arrayMoving[0];

if (!canMovingSide(whiteMoving)) {
	return false;
}

if (arrayMoving.length <= 1) {
	alert("Нет хода чёрных.");
	return false;
}

var blackMoving = arrayMoving[1];

if (!canMovingSide(blackMoving)) {
	return false;
}

return true;
}

function canMovingSide(moving) {
if (!clickChekerDecoder(moving)) {
	return false;
}
var isGreenMove;
if (moving[2] == '-') {
	isGreenMove = true;
} else if (moving[2] == ':') {
	isGreenMove = false;
} else {
	alert("Некорректно указано действие шашки - или : .");
	return false;
}

moving = cutCell(moving);
if (!doMovingDecoder(isGreenMove, moving.slice(0,2))) {
	return false;
}

if (moving.length > 2 && !canMovingSide(moving)) {
	return false;
}

finishMove();
return true;
}

function clickChekerDecoder(moving) {
var regexCell = /^[A-H]/;
//var regexGreenMoving = /^[A-H][1-8]-[A-H][1-8]/;
//var regexRedMoving = /(^[A-H][1-8]:)+([A-H][1-8])+?/;


//достаем шашку с ячейки
var cellCheker;

//выделяем шашку
	cellCheker = moving.slice(0,2);
	if (cellCheker.search(regexCell) == -1) {
		alert("Неправильно указана ячейка.");
		return false;
	}
	var col = getCol(cellCheker);
	var str = 8 - cellCheker[1];

	if (board[str][col] == "") {
		alert("В этой ячейке ( " + cellCheker + " ) нет шашки.");
		return false;
	}

if(!isCheked) {
	clickCheker(board[str][col]);
}

return true;
}

function doMovingDecoder(isGreenMoving, cellId) {
var cell = document.getElementById(cellId);

var colorGreen = "rgb(127, 255, 212)";
var colorRed = "rgb(255, 99, 71)";

var cannotGreenMoving = (isGreenMoving && (cell.style.backgroundColor != colorGreen));
var cannotRedMoving = (!isGreenMoving && (cell.style.backgroundColor != colorRed));

if (cannotGreenMoving) {
	alert("Нельзя сделать такой ход в текущем положении.");
		return false;
}

if (cannotRedMoving) {
	alert("Нельзя сделать такой ход в текущем положении.");
		return false;
}

clickCell(cellId);
return true;
}

function cutCell(moving) {
	return moving.slice(3, moving.length)
}
