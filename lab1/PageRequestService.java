package lab1;

import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/*1. При помощи программы на Java с использованием java.net.Socket запросить с сайта страницу по указанному URL.
   URL страницы должен указываться пользателем. Тело страницы сохранить в файл.
   Заголовки, отданные сервером, вывести в консоль.
 2. То же самое, но при помощи класса java.net.URLConnection*/

public class PageRequestService {
    public static void getInfoBySocket(String server, String filePath) throws IOException {
        try (Socket socket = new Socket(server, 80)) {
            OutputStream outStream = socket.getOutputStream();
            InputStream inStream = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));
            PrintWriter writer = new PrintWriter(new BufferedOutputStream(outStream));

            writer.print("GET / HTTP/1.1\nHost:" + server + "\n\n");
            writer.flush();
            String line = reader.readLine();
            Boolean isHeader = true;


            try (PrintWriter printWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(filePath)))) {
                while (line != null) {
                    System.out.println(line);
                    if ("".equals(line)) {
                      //  isHeader = false;
                    }
                    if (isHeader) {
                        //System.out.println(line);
                    } else {
                      //  printWriter.println(line);
                    }
                    line = reader.readLine();
                }
            }
        }
    }

    public static void getInfoByURL(String server, String filePath) throws IOException {
        URL url = new URL(server);
        URLConnection urlCon = url.openConnection();
        Map<String, List<String>> header = urlCon.getHeaderFields();

        for (Map.Entry<String, List<String>> mp : header.entrySet()) {
            System.out.print(mp.getKey() + " : ");
            System.out.println(mp.getValue().toString());
        }

        try (PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(filePath)));
             BufferedInputStream input = new BufferedInputStream(urlCon.getInputStream())) {
            int nextByte = 0;
            while (((nextByte = input.read()) != -1)) {
                printWriter.write(nextByte);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        String server1 = "bugred.ru";
        String server2 = "http://bugred.ru/";
        String path1 = "socketLog.html";
        String path2 = "URLLog.html";
        getInfoBySocket(server1, path1);
       // System.out.println("--------------------------------------------------------");
        //getInfoByURL(server2, path2);
    }
}
